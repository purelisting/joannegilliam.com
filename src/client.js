import React, {Component} from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import Store from './Store/Store';
import SmoothScroll from 'smooth-scroll';

import App from './Components/App/App';

const ss = new SmoothScroll();
render(
  <Provider store={Store}>
    <App />
  </Provider>,
  document.getElementById('rootApp'),
  ss.init()
);
